import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:scorecounter/view/score_view.dart';

class Home extends StatefulWidget {
  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  TextEditingController _team1Controller = TextEditingController();
  TextEditingController _team2Controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final String assetName = 'assets/img/goal.svg';
    final Widget svg =
        SvgPicture.asset(assetName, semanticsLabel: 'Goal Image');

    isControllerEmpty() {
      return _team1Controller.text != '' && _team2Controller.text != '';
    }

    @override
    void dispose() {
      _team1Controller.dispose();
      _team2Controller.dispose();
    }

    var screen = MediaQuery.of(context).size;
    final node = FocusScope.of(context);
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: Column(
        children: [
          Container(
            height: 100,
            width: screen.width,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [
                  const Color(0xFF6C63FF),
                  const Color(0xFF6389ff),
                ],
              ),
              borderRadius: BorderRadius.vertical(
                  bottom: Radius.elliptical(screen.width, 50.0)),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                SafeArea(
                  child: Text(
                    'Add Team',
                    style: TextStyle(fontSize: 20),
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.only(left: 10, right: 10),
                child: Column(
                  children: [
                    Container(
                      margin: EdgeInsets.only(top: 10),
                      width: screen.width / 1.3,
                      height: screen.height / 3,
                      child: svg,
                    ),
                    Container(
                      width: screen.width / 1.2,
                      child: TextField(
                        decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: 'Add Team 1',
                        ),
                        controller: _team1Controller,
                        onEditingComplete: () => node.nextFocus(),
                        style: TextStyle(),
                      ),
                    ),
                    SizedBox(height: 20),
                    Container(
                      width: screen.width / 1.2,
                      child: TextField(
                        decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: 'Add Team 2',
                        ),
                        controller: _team2Controller,
                        onEditingComplete: () => node.unfocus(),
                        style: TextStyle(),
                      ),
                    ),
                    SizedBox(height: 20),
                    FloatingActionButton(
                      onPressed: isControllerEmpty()
                          ? () => Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => ScoreView(
                                      team1: _team1Controller.text,
                                      team2: _team2Controller.text),
                                ),
                              )
                          : null,
                      backgroundColor:
                          isControllerEmpty() ? Color(0xFF6389ff) : Colors.grey,
                      elevation: 0,
                      child: Icon(Icons.chevron_right),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
