import 'package:flutter/material.dart';

class ScoreView extends StatefulWidget {
  final String team1;
  final String team2;
  ScoreView({this.team1, this.team2});

  @override
  _ScoreViewState createState() => _ScoreViewState();
}

class _ScoreViewState extends State<ScoreView> {
  int _team1Score = 0;
  int _team2Score = 0;

  @override
  Widget build(BuildContext context) {
    String teamName1 = widget.team1.toString();
    String teamName2 = widget.team2.toString();

    @override
    var screen = MediaQuery.of(context).size;

    return Scaffold(
      body: Column(
        children: [
          Container(
            height: 100,
            width: screen.width,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [
                  const Color(0xFF6C63FF),
                  const Color(0xFF6389ff),
                ],
              ),
              borderRadius: BorderRadius.vertical(
                  bottom: Radius.elliptical(screen.width, 50.0)),
            ),
            child: SafeArea(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  IconButton(
                    splashColor: Colors.transparent,
                    highlightColor: Colors.transparent,
                    icon: Icon(Icons.chevron_left),
                    onPressed: () => Navigator.pop(context),
                  ),
                  SizedBox(width: 30),
                  Text(
                    'Score Counter',
                    style: TextStyle(fontSize: 20),
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Container(
                  height: screen.height / 2 - 100,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Container(
                        width: screen.width / 2,
                        decoration: BoxDecoration(
                            gradient: LinearGradient(colors: [
                              const Color(0xFF6C63FF),
                              const Color(0xFF6389ff),
                            ]),
                            borderRadius: BorderRadius.circular(20)),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Text(
                              teamName1,
                              style: TextStyle(fontSize: 20),
                            ),
                            Text(
                              _team1Score.toString(),
                              style: TextStyle(fontSize: 50),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            FloatingActionButton(
                              heroTag: 'team1Increment',
                              mini: true,
                              child: Icon(Icons.add),
                              onPressed: () => setState(() => _team1Score++),
                            ),
                            FloatingActionButton(
                              heroTag: 'team1Decrement',
                              mini: true,
                              child: Icon(Icons.remove),
                              onPressed: () => setState(
                                () => {
                                  if (_team1Score > 0) {_team1Score--}
                                },
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  height: screen.height / 2 - 100,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Container(
                        width: screen.width / 2,
                        decoration: BoxDecoration(
                            gradient: LinearGradient(
                              colors: [
                                const Color(0xFF6C63FF),
                                const Color(0xFF6389ff),
                              ],
                            ),
                            borderRadius: BorderRadius.circular(20)),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Text(
                              teamName2,
                              style: TextStyle(fontSize: 20),
                            ),
                            Text(
                              _team2Score.toString(),
                              style: TextStyle(fontSize: 50),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            FloatingActionButton(
                              heroTag: 'team2Increment',
                              mini: true,
                              child: Icon(Icons.add),
                              onPressed: () => setState(() => _team2Score++),
                            ),
                            FloatingActionButton(
                              heroTag: 'team2Decrement',
                              mini: true,
                              child: Icon(Icons.remove),
                              onPressed: () => setState(
                                () => {
                                  if (_team2Score > 0) {_team2Score--}
                                },
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
